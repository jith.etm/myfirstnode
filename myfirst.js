var http = require('http');
var url = require('url');
var fs = require('fs');
var uc = require('upper-case');
var events = require('events');
var formidable = require('formidable');
var nodemailer = require('nodemailer');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb"; 

// var dt = require('./myfirstmodule');
// var adr = 'http://localhost:8080/default.htm?year=2017&month=march';
// var q = url.parse(adr, true);

http.createServer(function (req, res) {
  // res.writeHead(200, {'Content-Type': 'text/html'});
  // res.write("The date and time are currently: " + dt.myDateTime());
  // res.write(req.url)
  // res.end();

  // var q = url.parse(req.url, true).query;
  // var txt = q.year + " " + q.month;
  // res.write(txt)
  // res.end()

  // fs.readFile('demo1.html', function(err, data) {
  //   res.writeHead(200, {'content-Type': 'text/html'});
  //   res.write(data);
  //   return res.end();
  // })

  // fs.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
  //   if (err) throw err;
  //   console.log('Saved!');
  // }); 

  // fs.open('mynewfile2.txt', 'w', function (err, file) {
  //   if (err) throw err;
  //   console.log('Saved!');
  // }); 

  // fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
  //   if (err) throw err;
  //   console.log('Saved!');
  // }); 

  // fs.appendFile('mynewfile1.txt', ' This is my text.', function (err) {
  //   if (err) throw err;
  //   console.log('Updated!');
  // }); 

  // fs.writeFile('mynewfile3.txt', 'This is my text', function (err) {
  //   if (err) throw err;
  //   console.log('Replaced!');
  // }); 

  // fs.unlink('mynewfile2.txt', function (err) {
  //   if (err) throw err;
  //   console.log('File deleted!');
  // }); 

  // fs.rename('mynewfile1.txt', 'myrenamedfile.txt', function (err) {
  //   if (err) throw err;
  //   console.log('File Renamed!');
  // }); 

  // console.log(q.host); //returns 'localhost:8080'
  // console.log(q.pathname); //returns '/default.htm'
  // console.log(q.search); //returns '?year=2017&month=february'

  // var qdata = q.query; //returns an object: { year: 2017, month: 'february' }
  // console.log(qdata.month);
  // res.write(qdata.month)
  // res.end()

  // var q = url.parse(req.url, true);
  // var filename = "." + q.pathname;
  // fs.readFile(filename, function(err, data) {
  //   if (err) {
  //     res.writeHead(404, {'Content-Type': 'text/html'});
  //     return res.end("404 Not found");
  //   }

  //   res.writeHead(200, {'Content-Type' : 'text/html'});
  //   res.write(data);
  //   return res.end()
  // })

  // res.writeHead(200, {'Content-Type': 'text/html'});
  // res.write(uc.upperCase("Hello World"));
  // res.end();

  // var eventEmitter = new events.EventEmitter();

  // var myEventHandler = function () {
  //   // body..
  //   console.log('i hear scream');
  //   res.writeHead(200, {'Content-Type' : 'text/html'});
  //   res.write('i hear scream');
  //   return res.end()
  // };

  // eventEmitter.on('scream', myEventHandler);

  // eventEmitter.emit('scream');

  // if(req.url == '/fileupload') {
  //   var form = new formidable.IncomingForm();
  //   form.parse(req, function(err, fields, files) {
  //     console.log(files)
  //     var oldpath = files.filetoupload.filepath;
  //     var newpath = '/home/pdm/Desktop/Node/' + files.filetoupload.originalFilename;
  //     fs.rename(oldpath, newpath, function (err) {
  //       if (err) throw err;
  //       res.write('File uploaded and moved!');
  //       res.end();
  //     });
  //   });
  // } else {
  //   res.writeHead(200, {'Content-Type': 'text/html'});
  //   res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
  //   res.write('<input type="file" name="filetoupload"><br>');
  //   res.write('<input type="submit">');
  //   res.write('</form>');
  //   res.end();
  // }

  // var transporter = nodemailer.createTransport({
  //   service: 'gmail',
  //   auth: {
  //     user: 'jith.etm@gmail.com',
  //     pass: 'yzcqfgljyjrsbsum'
  //   }
  // });

  // var mailOptions = {
  //   from: 'jith.etm@gmail.com',
  //   to: 'abhijith.purushan@brtchip.com',
  //   subject: 'Sending Email using Node.js',
  //   text: 'That was easy!'
  // };

  // transporter.sendMail(mailOptions, function(error, info){
  //   if (error) {
  //     console.log(error);
  //   } else {
  //     console.log('Email sent: ' + info.response);
  //     res.writeHead(200, {'Content-Type': 'text/html'});
  //     res.write("Email sent");
  //     res.end();
  //   }
  // }); 

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    console.log("Database created!");
    db.close();
  });
}).listen(8080); 