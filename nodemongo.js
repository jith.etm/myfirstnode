var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

// http.createServer(function (req, res) {
	// res.writeHead(200, {'Content-Type': 'text/html'});
	// res.write("Hello ");
	// res.end();

	// MongoClient.connect(url, function(err, db) {
	// 	res.writeHead(200, {'Content-Type': 'text/html'});
	//   if (err) throw err;
	//   var dbo = db.db("mydb");
	//   dbo.createCollection("customers", function(err, res) {
	//     if (err) throw err;
	//     console.log("Collection created!");
	//     res.write("Collection created!");
	// 	res.end();
	//     db.close();
	//   });
	// }); 
// }).listen(8080);

MongoClient.connect(url, async function(err, db) {
	if (err) throw err;
	var dbo = db.db("mydb");
	var collections = await dbo.listCollections().toArray();
	console.log(collections.length);
	if (collections.length == 0) {
		dbo.createCollection("customers", function(err, res) {
			if (err) throw err;
			console.log("Collection created!");
			db.close();
		});	
	} else {
		// var myobj = { name: "Company Inc", address: "Highway 37" };
		// dbo.collection("customers").insertOne(myobj, function(err, res) {
		//     if (err) throw err;
		//     console.log("1 document inserted");
		//     db.close();
		// });

		 // var myobj = [
		 //    { name: 'John', address: 'Highway 71'},
		 //    { name: 'Peter', address: 'Lowstreet 4'},
		 //    { name: 'Amy', address: 'Apple st 652'},
		 //    { name: 'Hannah', address: 'Mountain 21'},
		 //    { name: 'Michael', address: 'Valley 345'},
		 //    { name: 'Sandy', address: 'Ocean blvd 2'},
		 //    { name: 'Betty', address: 'Green Grass 1'},
		 //    { name: 'Richard', address: 'Sky st 331'},
		 //    { name: 'Susan', address: 'One way 98'},
		 //    { name: 'Vicky', address: 'Yellow Garden 2'},
		 //    { name: 'Ben', address: 'Park Lane 38'},
		 //    { name: 'William', address: 'Central st 954'},
		 //    { name: 'Chuck', address: 'Main Road 989'},
		 //    { name: 'Viola', address: 'Sideway 1633'}
		 //  ];
		 //  dbo.collection("customers").insertMany(myobj, function(err, res) {
		 //    if (err) throw err;
		 //    console.log("Number of documents inserted: " + res.insertedCount);
		 //    db.close();
		 //  });

		 // var myobj = [
		 //    { _id: 154, name: 'Chocolate Heaven'},
		 //    { _id: 155, name: 'Tasty Lemon'},
		 //    { _id: 156, name: 'Vanilla Dream'}
		 //  ];
		 //  dbo.collection("products").insertMany(myobj, function(err, res) {
		 //    if (err) throw err;
		 //    console.log(res);
		 //    db.close();
		 //  });

		// dbo.collection("customers").findOne({}, function(err, result) {
		//     if (err) throw err;
		//     console.log(result.name);
		//     db.close();
		// });

		  // dbo.collection("customers").find({}).toArray(function(err, result) {
		  //   if (err) throw err;
		  //   console.log(result);
		  //   db.close();
		  // });

		 // dbo.collection("customers").find({}, { projection: { _id: 0, name: 1, address: 1 } }).toArray(function(err, result) {
		 //    if (err) throw err;
		 //    console.log(result);
		 //    db.close();
		 //  });

		// var query = { address: "Park Lane 38" };
		//   dbo.collection("customers").find(query).toArray(function(err, result) {
		//     if (err) throw err;
		//     console.log(result);
		//     db.close();
		//   });

		var mysort = { name: 1 };
		  dbo.collection("customers").find().sort(mysort).toArray(function(err, result) {
		    if (err) throw err;
		    console.log(result);
		    db.close();
		  });
	}

}); 